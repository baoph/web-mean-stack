exports.NOT_FOUND = "Not found"
exports.SERVER_ERROR = "An error occurred on the server, please try later";
exports.INVALID_VALIDATE = "VALIDATE ERROR, PLEASE CHECK YOUR ENTERED DATA";
exports.INSERT_SUCCESSFUL = "Success to create new ";
exports.UNAUTHORIZED = "You are not authorize for this action ";
exports.NOT_SELECTED = "You need to select one to perform this action";
exports.NOT_ACTIVE = "Status is inactive, please contact to admin";
exports.WRONG_INFO = "You have entered a wrong email or password, please check and enter again";


