exports.isEmptyObject = function (val) {
    return val != null &&
        typeof val === 'object' &&
        Object.keys(val).length === 0;
};

exports.isLengthAppropriate = function (val) {
    if (val.length >= 6) {
        return true;
    }
    return false;
};

exports.checkProperties = function (obj) {
    Object.keys(obj).forEach(key => {
        if (!!!obj[key]) {
            return false;
        }
    })
    return true;
}

exports.validateOrder = function (obj) {
    Object.keys(obj).forEach(key => {
        if (!!!obj[key] && key !== 'note') {
            return false;
        }
    })
    return true;
}

exports.validateArticle = function (article) {
    if (!!article.articleContent && this.isLengthAppropriate(article.articleContent) && !!article.restaurant) {
        return true;
    }
    return false;
}

exports.errorResponse = function (res, status = 400, message) {
    res.set('Cache-Control', 'no-cache')
        .status(status)
        .json({code: status, message: message});
}

exports.successResponse = function (res, message) {
    res.set('Cache-Control', 'no-cache')
        .status(200)
        .json({code: 200, message: message});
}

exports.successResponseWithData = function (res, data) {
    res.set('Cache-Control', 'no-cache')
        .status(200)
        .json({code: 200, data: data});
}

module.exports
