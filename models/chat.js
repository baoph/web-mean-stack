const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment = require('moment');
moment.locale("vi");

const ChatSchema = mongoose.Schema({
    user: {
        type: ObjectId,
        ref: 'users',
        required: true
    },
    restaurant: {
        type: ObjectId,
        ref: 'restaurants',
        required: true
    },
});

const Chat = module.exports = mongoose.model('chats', ChatSchema);

module.exports.addNewChat = function (newChat, callback) {
    newChat.save(callback);
};
module.exports.getAllChat = function (callback) {
    Chat.find({}, callback).populate({
        path: 'user',
        path: 'restaurant',
    });
};

module.exports.findChat = function (text, callback) {
    // Chat.find(
    //     { $text : { $search : text} },
    //     { score : { $meta: "textScore" } }
    // ).sort({ score : { $meta : 'textScore' } }).exec(callback);
    Chat.find(
        {resName: {$regex: text, $options: 'i'}}, null, null, callback
    )
};

module.exports.getChatById = function (id, callback) {
    Chat.find({_id: id}, callback).populate({
        path: 'user',
        path: 'restaurant',
    })
};

module.exports.getChatId = function (params, callback) {
    Chat.find({user: params.user, restaurant: params.restaurant}, callback);
};

module.exports.getChatByUserId = function (id, callback) {
    Chat.find({user: id}, callback);
};


