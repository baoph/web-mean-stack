const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
const moment = require('moment');
const category = require('../model/category');
const article = require('../model/article');
moment.locale("vi");
const updatedSchema = mongoose.Schema({
    updatedAt: {
        type: String,
        default: moment().format("LLLL")

    },
    updatedBy: {
        type: ObjectId
    }
}, { _id: false });


const facultySchema = mongoose.Schema({
    facultyName: {
        type: String,
        required: true,
        unique:true
    },
    facultyDesc: {
        type: String,
        required: true,
        text: true
    },     
    created_At: {
        type: String,
        //default: moment().format("MMMM Do YYYY, h:mm:ss a")
    },
    created_By: {
        type: ObjectId,
        ref: 'Users',
        required: true
    },
    updated: [updatedSchema]
});

const Faculty = module.exports = mongoose.model('faculties', facultySchema);

module.exports.addNewFaculty = function (newFaculty, callback) {
    newFaculty.save(callback);
};

module.exports.updateFaculty = function (updateFaculty, callback) {
    //console.log(updateFaculty);
    Faculty.findOneAndUpdate({ _id: updateFaculty.facultyId }, {
        $set: {
            'facultyName': updateFaculty.facultyName,
            'facultyDesc': updateFaculty.facultyDesc
        }, $push: {
            updated: { updatedAt: updateFaculty.updatedAt, updatedBy: updateFaculty.updatedBy }
        }
    }, { multi: true }, callback);
};
module.exports.getAllFaculty = function (callback) {
    Faculty.find({}, callback).populate("created_By");
};

module.exports.getFacultyById = function (id, callback) {
    Faculty.find({_id:ObjectIdLog(id)}, callback).populate("created_By");
};

function getStatisticByFaculty(id,closureId,callback) {
    category.getByFaculty(id,(err,categories)=>{
        let listCategories = [];
        for (let category of categories) {
            listCategories.push(category._id)
        }
        article.findByArrayCategory(listCategories,closureId,function (err,article) {
            callback(null,{faculty:id,article:article})
        })

    })
};

module.exports.getAllFacultyIdStatistic = function (closureid,callback) {
    Faculty.find().distinct('_id',function (err,faculties) {

        for (let faculty of faculties) {
            (function (facul) {
                getStatisticByFaculty(facul,closureid,(err,articles)=>{
                    return articles;
                })
            })(faculty)

        }
    });
}

