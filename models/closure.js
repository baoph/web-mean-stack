const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const moment_tz = require('moment-timezone');
const moment = require('moment');
const multer = require('multer');
moment.locale("vi");


const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");

const updatedSchema = mongoose.Schema({
    updatedAt: {
        type: String,
        //default: moment().format("MMMM Do YYYY, h:mm:ss a")
    },
    updatedBy: {
        type: ObjectId,
        ref: 'Users'
    }
}, { _id: false });
const closureSchema = mongoose.Schema({    
    status: {
        type: Number,
         // 1: Active, 0: Disable
    },  
    created_At: {
        type: String,
        default: moment().format("MMMM Do YYYY, h:mm:ss a")
    },
    expired_At: {
        type: String,
        default: moment().format("MMMM Do YYYY, h:mm:ss a")
    },
    expired_Edit: {
        type: String,
        default: moment().format("MMMM Do YYYY, h:mm:ss a")
    },
    created_By: {
        type: ObjectId,
        ref: 'Users'
    },
    username: {
        type: String
    },
    updated: [updatedSchema]
});

const Closure = module.exports = mongoose.model('Closure', closureSchema);
module.exports.addNewClosure = function (NewClosure, callback) {
    NewClosure.save(callback);
};
module.exports.getClosureById = function (id, callback) {
    Closure.findById(id, callback);
};
module.exports.editClosure = function (closure, callback) {
    Closure.findOneAndUpdate({ _id: closure.closureid }, {
        $set: {
            'created_At': closure.created_At,
            'expired_At': closure.expired_At,
            'status': closure.status,
            'expired_Edit': closure.expired_Edit
        }, $push: {
            updated: { updatedAt: closure.updatedAt, updatedBy: closure.updatedBy }
        }
    }, { multi: true }, callback);   
};
module.exports.getCurrentActive = function (callback) {
    var now = moment().format("MMMM Do YYYY, h:mm:ss a");
    Closure.find({ status: 1 }, function (err, closure) {
        if (err) callback(err, null);
        else {        
            if (closure.length < 1) {
                callback('Hiện tại không có closure nào đang active, vui lòng thử lại sau!', null);
            } else {
                if (closure[0].expired_At < now) {
                    console.log("false");
                    callback("The deadline is overdue! You can not submit anymore!", null);                                 
                }
            
                else {
                    console.log("true");
                    callback(null, closure[0]._id);

                }
            }
        }
    })
};
module.exports.getOne = function (callback) {
    Closure.find({ status: 1 }, callback);
};
module.exports.getCurrentActiveEdit = function (id, callback) {
    var now = moment().format("MMMM Do YYYY, h:mm:ss a");
    Closure.find({ _id: id }, function (err, closure) {
        if (err) throw (err, null);
        else {        
            if (closure.length < 1) {
                callback('Hiện tại không có closure nào đang active, vui lòng thử lại sau!', null);
            } else {
                if (closure[0].expired_Edit < now) {
                    callback("The deadline is overdue! You can not edit anymore!", null);
                }
                else {
                    //console.log("true");
                    callback(null, closure[0]._id);
                }
            }
        }
    })
};
module.exports.getCurrentActiveInfor = function (callback) {
    Closure.find({ status: 1 }, callback)
};
