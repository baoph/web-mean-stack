const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment = require('moment');
moment.locale("vi");

const updatedSchema = mongoose.Schema({
    updatedAt: {
        type: String
    },
    updatedBy: {
        type: ObjectId,
        ref: 'Users'
    }
}, {_id: false});

const RestaurantSchema = mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    tel: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    resName: {
        type: String,
        required: true,
        unique: true
    },
    resAddress: {
        type: String,
        required: true,
        unique: true
    },
    article: {
        type: ObjectId,
        ref: 'articles'
    },
    createdAt: {type: String},
    updated: [updatedSchema],
    status: {
        type: Number,
        default: 1 //active
    }
});

const Restaurant = module.exports = mongoose.model('restaurants', RestaurantSchema);

module.exports.addNewRestaurant = function (newCategory, callback) {
    newCategory.save(callback);
};
module.exports.getAllRestaurant = function (callback) {
    Restaurant.find({}, callback).populate('article');
};
module.exports.findRestaurant = function (text, callback) {
    // Restaurant.find(
    //     { $text : { $search : text} },
    //     { score : { $meta: "textScore" } }
    // ).sort({ score : { $meta : 'textScore' } }).exec(callback);
    Restaurant.find(
        {resName: { $regex: text, $options: 'i' }}, null, null, callback
    )
};
module.exports.getRestaurantById = function (id, callback) {
    Restaurant.find({_id: id}, callback).populate({
        path: 'article',
        populate: {
            path: 'category',
        }
    });
};
module.exports.updateRestaurant = function (updateRestaurant, callback) {
    Restaurant.findOneAndUpdate({_id: updateRestaurant.restaurantId}, {
        $set: {
            'username': updateRestaurant.username,
            'tel': updateRestaurant.tel,
            'email': updateRestaurant.email,
            'resName': updateRestaurant.resName,
            'resAddress': updateRestaurant.resAddress,
            'article': updateRestaurant.article
        }, $push: {
            updated: {updatedAt: updateRestaurant.updatedAt, updatedBy: updateRestaurant.updatedBy}
        }
    }, {multi: true}, callback);
};

