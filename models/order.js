const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment = require('moment');
moment.locale("vi");

const updatedSchema = mongoose.Schema({
    updatedAt: {
        type: String
    },
    updatedBy: {
        type: ObjectId,
        ref: 'Users'
    }
}, {_id: false});

const OrderSchema = mongoose.Schema({
    orderDate: {
        type: Date,
        required: true
    },
    orderTime: {
        type: String,
        required: true
    },
    adult: {
        type: Number,
        required: true,
        min: 1,
        max: 100
    },
    children: {
        type: Number,
        required: true,
        min: 0,
        max: 100
    },
    user: {
        type: ObjectId,
        ref: 'users'
    },
    username: {
        type: String,
    },
    email: {
        type: String,
    },
    tel: {
        type: String,
    },
    note: {
        type: String,
    },
    restaurant: {
        type: ObjectId,
        ref: 'restaurants'
    },
    createdAt: {type: String},
    updated: [updatedSchema],
    status: {
        type: Number,
        default: 1 //active
    }
});

const Order = module.exports = mongoose.model('orders', OrderSchema);

module.exports.addNewOrder = function (newCategory, callback) {
    newCategory.save(callback);
};
module.exports.getAllOrder = function (callback) {
    Order.find({}, callback);
};
module.exports.findOrder = function (text, callback) {
    // Order.find(
    //     { $text : { $search : text} },
    //     { score : { $meta: "textScore" } }
    // ).sort({ score : { $meta : 'textScore' } }).exec(callback);
    Order.find(
        {resName: '/' + text + '/i'}, null, null, callback
    )
};
module.exports.getOrderById = function (id, callback) {
    Order.findById(id, callback);
};
module.exports.updateOrder = function (updateOrder, callback) {
    console.log(updateOrder);
    Order.findOneAndUpdate({_id: updateOrder.categoryId}, {
        $set: {
            'tel': updateOrder.resName,
            'email': updateOrder.resName,
            'resName': updateOrder.resName,
            'resAddress': updateOrder.resAddress
        }, $push: {
            updated: {updatedAt: updateOrder.updatedAt, updatedBy: updateOrder.updatedBy}
        }
    }, {multi: true}, callback);
};

