const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment = require('moment');
moment.locale("vi");

const updatedSchema = mongoose.Schema({
    updatedAt: {
        type: String
    },
    updatedBy: {
        type: ObjectId,
        ref: 'Users'
    }
}, {_id: false});

const EventSchema = mongoose.Schema({
    eventName: {
        type: String,
        required: true,
        unique: true
    },
    type: {
        type: Number,
        default: 1 // 1: event, 2: payment
    },
    createdAt: {type: String},
    updated: [updatedSchema],
    status: {
        type: Number,
        default: 1 //active
    }
});

const Event = module.exports = mongoose.model('events', EventSchema);

module.exports.addNewEvent = function (newCategory, callback) {
    newCategory.save(callback);
};
module.exports.getAllEvent = function (callback) {
    Event.find({}, callback);
};

module.exports.getEventById = function (id, callback) {
    Event.findById(id, callback);
};


