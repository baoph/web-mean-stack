const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const moment_tz = require('moment-timezone');
mongoose.Promise = global.Promise;
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
const moment = require('moment');
let mammoth = require("mammoth");
moment.locale("vi");

const ArticleSchema = mongoose.Schema({
    articleImage: {
        type: Object
    },
    articleContent: {
        type: Object,
        required: true,
        text: true
    },
    priceRange: {
        type: Object,
        required: true
    },
    category: {
        type: ObjectId,
        ref: 'categories'
    },
    created_At: {
        type: String,
        //default:moment().format("MMMM Do YYYY, h:mm:ss a")
    },
    created_By: {
        type: ObjectId,
        ref: 'Users'
    },
    approved_By: {
        type: ObjectId,
        ref: 'Users'
    },
});

const Articles = module.exports = mongoose.model('articles', ArticleSchema);

function removeItem(array, itemToRemove) {
    return array.filter(item => item != itemToRemove);
}

// module.exports.upOrDownVote = function (articleUpVote, articleDownVote, userId) {
//     let upVote = articleUpVote.find(function (element) {
//         return element == userId;
//     });
//     let downVote = articleDownVote.find(function (element) {
//         return element == userId;
//     });
//     if (upVote !== undefined) {
//         return 1;
//     } else if (downVote !== undefined) {
//         return 2;
//     } else {
//         return 0;
//     }
// };

module.exports.insertUploadedFile = function (newArticle, callback) {
    newArticle.save(callback);
};
module.exports.findArticle = function (text, callback) {
    Articles.find(
        {$text: {$search: text}},
        {score: {$meta: "textScore"}}
    ).sort({score: {$meta: 'textScore'}}).exec(callback);
};
//module.exports.getArticleByFaculty = function (id, callback) {
//console.log(updateArticleStatus);

//Articles.find().populate('created_By').populate({
//    path: 'articleCategory',
//    populate: {
//        path: "belongto_faculty",
//        match: { _id: id }
//    }
//}).exec(function (err, articles) {
//    if (err) callback('errr', null);
//    else {
//        articles = articles.filter(function (resul) {
//            var newres = Object.values(resul);
//            console.log(newres[3].articleCategory.belongto_faculty);
//            if (newres[3].articleCategory.belongto_faculty == null) {
//                newres[3] = null;
//            }

//            callback(null, newres[3])
//        });
//    }
//})};
module.exports.updateArticleStatus = function (updateArticleStatus, callback) {
    //console.log(updateArticleStatus);
    Articles.findOneAndUpdate({_id: updateArticleStatus.articleId}, {
        $set: {
            'status': updateArticleStatus.status
        }, $push: {
            updated: {updatedAt: updateArticleStatus.updatedAt, updatedBy: updateArticleStatus.updatedBy}
        }
    }, {multi: true}, callback);
};

module.exports.editArticle = function (editArticle, callback) {
    // console.log(editArticle);
    Articles.findOneAndUpdate({_id: editArticle._id}, {
            $set: {
                'articleContent': editArticle.articleContent,
                'articleImage': editArticle.articleImage,
                'priceRange': editArticle.priceRange,
                'category': editArticle.category
            }, $push:
                {
                    updated: {
                        updatedAt: editArticle.updatedAt,
                        updatedBy: editArticle.updatedBy
                    }
                }
        },
        {
            multi: true
        },
        callback
    );
};

module.exports.getArticleByRestaurant = function (pageOptions, resId, callback) {
    Articles.find({
        articleRestaurant: resId,
        status: "2"
    }).populate('created_By').sort({_id: -1}).skip(pageOptions.page * pageOptions.limit)
        .limit(pageOptions.limit).exec(callback);
};

module.exports.getAllArticlesOrderByCreatedDate = function (pageOptions, callback) {
    Articles.find({status: "2"}).populate('created_By').sort({_id: -1}).skip(pageOptions.page * pageOptions.limit)
        .limit(pageOptions.limit).exec(callback);
};

module.exports.docxToHTML = async function (files, callback) {
    for (let file of files) {
        let arr = file.originalname.split('.');
        let outputLast = arr.pop();
        if (outputLast === 'docx') {
            mammoth.convertToHtml({path: file.path})
                .then(function (result) {
                    let html = result.value;
                    callback(null, html);
                })
                .done();
            return false;
        }
    }
    ;
    callback(true, null);
};
module.exports.getAllImage = function (files) {
    let image = [];
    for (let file of files) {
        let arr = file.originalname.split('.');
        let outputLast = arr.pop();

        if (outputLast === 'png' || outputLast === 'jpg' || outputLast === 'jpeg' || outputLast === 'gif') {
            image.push(file.path);
        }
    }
    return image;
};

module.exports.getDoc = function (files) {
    let doc = [];
    for (let file of files) {
        let arr = file.originalname.split('.');
        let outputLast = arr.pop();

        console.log(outputLast);
        if (outputLast === 'doc' || outputLast === 'docx') {
            doc.push(file.path);
        }
    }
    return doc;
};

module.exports.getArticleById = function (id, callback) {
    Articles.find({_id: id}, callback).populate('category');
};
module.exports.getArticleApprovementById = function (id, callback) {
    Articles.findById(id, callback).populate('created_By').populate({
        path: 'comment',
        match: {cmt_status: 1},
        populate: {
            path: "subComment",
            populate: {path: 'comment_user'}
        }
    }).populate({
        path: 'comment',
        match: {cmt_status: 1},
        populate: {
            path: 'comment_user'
        },
        options: {sort: {'commented_At': -1}}
    });

};
module.exports.getUserArticle = function (id, callback) {
    Articles.find({created_By: id}, callback).populate('created_By');
};
//module.exports.countAllArticles = function (id, callback) {
//    Articles.aggregate([{
//        $match: {
//            status: 1
//        }
//    },
//    {
//        $group: {

//        }
//    }], function (err, data) {
//        if (err) {
//            res.send(err);
//            return;
//        }
//        console.log(data);
//        res.send({ data });
//    });
//};
module.exports.upVoteArticle = async function (userId, articleId, callback) {
    await Articles.findOne({_id: articleId}, async (err, article) => {
        if (err) callback(err, null);
        else {
            let newUpVote = removeItem(article.upVote, userId);
            let newDownVote = removeItem(article.downVote, userId);
            console.log(newUpVote);
            console.log(newDownVote);
            Articles.findOneAndUpdate({_id: articleId}, {
                $set: {
                    upVote: newUpVote,
                    downVote: newDownVote
                }
            }, function (err, detail) {
                if (err) console.log(err);
            });
        }
    });

    Articles.findOneAndUpdate({_id: articleId}, {
        $push: {
            upVote: userId
        }
    }, callback);
};
module.exports.downVoteArticle = async function (userId, articleId, callback) {
    await Articles.findOne({_id: articleId}, async (err, article) => {
        if (err) callback(err, null);
        else {
            let newUpVote = removeItem(article.upVote, userId);
            let newDownVote = removeItem(article.downVote, userId);
            console.log(newUpVote);
            Articles.findOneAndUpdate({_id: articleId}, {
                $set: {
                    upVote: newUpVote,
                    downVote: newDownVote
                }
            }, function (err, detail) {
                if (err) console.log(err);
            });
        }
    });
    Articles.findOneAndUpdate({_id: articleId}, {
        $push: {
            downVote: userId
        }
    }, callback);
};
module.exports.addComment = function (commentId, articleId, callback) {
    Articles.findOneAndUpdate({_id: articleId}, {
        $push: {
            comment: commentId
        }
    }, callback);
};

module.exports.statistic = function (closure_id, callback) {
    Articles.aggregate([
        {$match: {closure_id: ObjectIdLog(closure_id)}},
        {
            $group: {
                _id: '$articleCategory',
                points: {$sum: 1}
            }
        }
    ], callback);
};

module.exports.totalArticleByClosure = function (closure_id, callback) {
    Articles.count({closure_id: ObjectIdLog(closure_id)}, callback)
};
module.exports.findByArrayCategory = function (category, closure_id, callback) {
    Articles.aggregate([
        {
            $match: {
                $and:
                    [
                        {'articleCategory': {$in: category}},
                        {'closure_id': ObjectIdLog(closure_id)}]
            }
        },
        {
            $group: {
                _id: '$articleCategory',
                points: {$sum: 1}
            }
        }
    ], callback)
};
module.exports.statisticByFaculty = function (closureId, callback) {
    Articles.aggregate([
        {$match: {'closure_id': ObjectIdLog(closureId)}},
        {$lookup: {from: "categories", localField: "articleCategory", foreignField: "_id", as: "category"}},
        {
            $group: {
                _id: '$category.belongto_faculty',
                points: {$sum: 1}
            }
        }
    ], callback)
};

module.exports.AllArticleByClosureDate = function (closureId, callback) {
    Articles.count([
        {$match: {'closure_id': ObjectIdLog(closureId)}}
    ], callback)
};

