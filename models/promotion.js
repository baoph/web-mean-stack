const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment = require('moment');
moment.locale("vi");

const updatedSchema = mongoose.Schema({
    updatedAt: {
        type: String
    },
    updatedBy: {
        type: ObjectId,
        ref: 'Users'
    }
}, {_id: false});

const SaleSchema = mongoose.Schema({
    promotionName: {
        type: String,
        required: true,
    },
    promotionPercent: {
        type: Number,
        required: true,
        min: 0,
        max: 100
    },
    promotionFrom: {
        type: String,
        required: true,
    },
    promotionTo: {
        type: Date,
        required: true,
    },
    restaurant: {
        type: ObjectId,
        ref: 'restaurants',
        required: true
    },
    createdAt: {type: String},
    updated: [updatedSchema],
    status: {
        type: Number,
        default: 1 //active
    }
});

const Promotion = module.exports = mongoose.model('promotions', SaleSchema);

module.exports.addNewPromotion = function (newCategory, callback) {
    newCategory.save(callback);
};
module.exports.getAllPromtions = function (callback) {
    Promotion.find({}, callback);
};
module.exports.findPromotionByName = function (text, callback) {
    // Promotion.find(
    //     { $text : { $search : text} },
    //     { score : { $meta: "textScore" } }
    // ).sort({ score : { $meta : 'textScore' } }).exec(callback);
    Promotion.find(
        {promotionName: '/' + text + '/i'}, null, null, callback
    )
};
module.exports.getPromotionByRestaurant = function (text, callback) {
    // Promotion.find(
    //     { $text : { $search : text} },
    //     { score : { $meta: "textScore" } }
    // ).sort({ score : { $meta : 'textScore' } }).exec(callback);
    Promotion.find(
        {restaurant: text}, null, null, callback
    )
};
module.exports.getPromotionById = function (id, callback) {
    Promotion.findById(id, callback);
};
module.exports.updatePromotion = function (updatePromotion, callback) {
    Promotion.findOneAndUpdate({_id: updatePromotion.categoryId}, {
        $set: {
            'promotionName': updatePromotion.promotionName,
            'promotionPercent': updatePromotion.resName,
            'resName': updatePromotion.resName,
            'resAddress': updatePromotion.resAddress
        }, $push: {
            updated: {updatedAt: updatePromotion.updatedAt, updatedBy: updatePromotion.updatedBy}
        }
    }, {multi: true}, callback);
};

