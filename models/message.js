const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment = require('moment');
moment.locale("vi");

const MessageSchema = mongoose.Schema({
    message: {
        type: String,
        required: true
    },
    createdAt: {type: String},
    chat: {
        type: ObjectId,
        ref: 'chats',
        required: true
    },
    sender: {
        type: ObjectId,
        required: true
    },
    receiver: {
        type: ObjectId,
        required: true
    }
});

const Message = module.exports = mongoose.model('messages', MessageSchema);

module.exports.addNewMessage = function (newMessage, callback) {
    newMessage.save(callback);
};

module.exports.getAllMessage = function (callback) {
    Message.find({}, callback).populate({
        path: 'chat',
    });
};

module.exports.findMessage = function (text, callback) {
    // Message.find(
    //     { $text : { $search : text} },
    //     { score : { $meta: "textScore" } }
    // ).sort({ score : { $meta : 'textScore' } }).exec(callback);
    Message.find(
        {resName: {$regex: text, $options: 'i'}}, null, null, callback
    )
};

module.exports.getMessageById = function (id, callback) {
    Message.find({_id: id}, callback).populate({
        path: 'user',
        path: 'restaurant',
    })
};

module.exports.getMessageByChatId = function (id, callback) {
    Message.find({chat: id}, callback).populate({
        path: 'chat',
        populate: {
            path: 'user',
            path: 'restaurant'
        }
    });
};


