const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment = require('moment');
moment.locale("vi");
const updatedSchema = mongoose.Schema({
    updatedAt: {
        type: String
    },
    updatedBy: {
        type: ObjectId,
        ref: 'users'
    }
}, {_id: false});

const UserSchema = mongoose.Schema({
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true

    },
    createdAt: {type: String},
    updated: [updatedSchema],

    role: {
        type: ObjectId,
        ref: 'roles'
    },
    restaurant: {
        type: ObjectId,
        ref: 'restaurants'
    },
    status: {
        type: Number,
        default: 1 //active

    }
});

const Users = module.exports = mongoose.model('users', UserSchema);

module.exports.addUser = function (newUser, callback) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
};
module.exports.getUserById = function (id, callback) {
    Users.findById(id, callback);
};
module.exports.getAllUser = function (id, callback) {
    Users.find({}, callback).populate("userFaculty");
};
module.exports.getGuestByFaculty = function (id, callback) {
    Users.find({userFaculty: id, role: "4"}, callback).populate("userFaculty");
};
module.exports.getCoordinatorByFaculty = function (id, callback) {
    Users.find({userFaculty: id, role: "3", status: "1"}, callback).populate("userFaculty");
};

module.exports.getUserByEmail = function (email, callback) {
    const query = {email: email};
    Users.findOne(query, callback);
};
module.exports.comparePassword = function (enteredPassword, hash, callback) {
    bcrypt.compare(enteredPassword, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
};

module.exports.editUser = function (editUser, callback) {
    //console.log(r);
    Users.findOneAndUpdate({_id: editUser.userId}, {
        $set: {
            'username': editUser.username,
            'email': editUser.email,
            'role': editUser.role,
            'status': editUser.status

        }, $push: {
            updated: {updatedAt: editUser.updatedAt, updatedBy: editUser.updatedBy}
        }
    }, {multi: true}, callback);
};
