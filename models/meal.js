const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment = require('moment');
moment.locale("vi");

const updatedSchema = mongoose.Schema({
    updatedAt: {
        type: String
    },
    updatedBy: {
        type: ObjectId,
        ref: 'Users'
    }
}, {_id: false});

const MealSchema = mongoose.Schema({
    mealName: {
        type: String,
        required: true,
        unique: true
    },
    createdAt: {type: String},
    updated: [updatedSchema],
    status: {
        type: Number,
        default: 1 //active
    }
});

const Meal = module.exports = mongoose.model('meals', MealSchema);

module.exports.addNewMeal = function (newCategory, callback) {
    newCategory.save(callback);
};
module.exports.getAllMeal = function (callback) {
    Meal.find({}, callback);
};

module.exports.getMealById = function (id, callback) {
    Meal.findById(id, callback);
};


