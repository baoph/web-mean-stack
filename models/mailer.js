const nodemailer = require('nodemailer');
const xoauth2 = require('xoauth2');
const mail_config = require('../config/mail_config');
const EmailTemplate = require('email-templates').EmailTemplate;
const Promise = require('bluebird');
const Email = require('email-templates');
const path = require('path');

let transpoter = nodemailer.createTransport({
    service:'gmail',
    auth: mail_config.auth
});

let mailOption ={
    from:'FptZine <FPT@fpt.edu.vn>',
    to: 'manhndgch16236@fpt.edu.vn',
    subject: 'NodeMailer Test',
    text: 'Hello world'
};
let SampleUsers = [{
    name:'Manh',
    email:'manhnd.jin@gmail.com'
},
    {
        name:'Manh',
        email:'manhnd.jin@gmail.com'
    },
    {
        name:'Manh',
        email:'manhnd.jin@gmail.com'
    }];

// transpoter.sendMail(mailOption,(err,res)=>{
//     if (err){
//         console.log(err)
//     }else{
//         console.log('Email Sent')
//     }
// });
function sendEmail(obj) {
    return transpoter.sendMail(obj);
}

function loadTemplate(templateName, contexts) {
     let template = new EmailTemplate(path.join(__dirname,'templates',templateName));
    return Promise.all(contexts.map((context)=>{
        return new Promise((resolve, reject) => {
            template.render(context, (err, result) => {
                if (err) reject(err);
                else resolve({ email: result, context })
            });
        });
    }));
}
module.exports.sendEmailNewArticles =function(toUsers) {
    loadTemplate('newArticles',toUsers).then((results)=>{
        return Promise.all(results.map((result) => {
            sendEmail({
                to: result.context.email,
                from: 'FPT MAGAZINE <manhndgch16236@fpt.edu.vn>',
                subject: result.email.subject,
                html: result.email.html,
                text: result.email.text,
            });
        }));
    }).then(() => {
        console.log('K');
    });
};
module.exports.sendEmailEditArticles = function (toUsers) {
    loadTemplate('EditArticles', toUsers).then((results) => {
        return Promise.all(results.map((result) => {
            console.log(result.context.email);
            sendEmail({
                to: result.context.email,
                from: 'FPT MAGAZINE <manhndgch16236@fpt.edu.vn>',
                subject: result.email.subject,
                html: result.email.html,
                text: result.email.text
            });
        }));
    }).then(() => {console.log('K');});
};