const utils = require("../utils/utils");
const msg = require('../utils/messages');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");
const Order = require('../models/order');

router.post('/createOrder', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let order = new Order({
        orderDate: req.body.orderDate,
        orderTime: req.body.orderTime,
        adult: req.body.adult,
        children: req.body.children,
        user: req.body.user,
        username: req.body.username,
        email: req.body.email,
        tel: req.body.tel,
        note: req.body.note,
        restaurant: req.body.restaurant,
    });
    if (utils.validateOrder(order)) {
        Order.addNewOrder(order, (err, newRes) => {
            if (err) {
                utils.errorResponse(res, 500, err);
            } else {
                utils.successResponse(res, msg.INSERT_SUCCESSFUL)
            }
        });
    } else {
        utils.errorResponse(res, msg.INVALID_VALIDATE);
    }
});

router.get('/getAllOrders', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Order.getAllOrder((err, orders) => {
        if (err) {
            utils.errorResponse(res, 500, msg.SERVER_ERROR);
        } else {
            utils.successResponseWithData(res, orders)
        }
    });
});

router.post('/getOrderInfo', (req, res, next) => {
    const id = req.body.id;

    Order.getOrderById(id, (err, order) => {
        if (err) throw err;

        if (!order) {
            utils.errorResponse(res, 404, msg.NOT_FOUND);
        } else {
            utils.successResponseWithData(res, order)
        }
    });
});

module.exports = router;
