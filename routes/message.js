const utils = require("../utils/utils");
const msg = require('../utils/messages');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require("../config/database");
const validate = require('../middleware/validate');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");
const Message = require('../models/message');
const fs = require('fs');
const multer = require('multer');


const storage = multer.diskStorage({
    destination: async function (req, file, cb) {
        // closure.getCurrentActive(async (err, closureId) => {
        let uploadTo = './public/uploads/messages';
        if (!fs.existsSync(uploadTo)) {
            await fs.mkdirSync(uploadTo);
            // console.log(uploadTo);
            cb(null, uploadTo);
        } else {
            // console.log(uploadTo);
            cb(null, uploadTo);
        }
        // });
    },
    filename: function (req, file, cb) {
        let arr = file.originalname.split('.');
        let outputLast = arr.pop();
        cb(null, Date.now() + '.' + outputLast);
    }
});
const uploadFolder = multer({storage: storage});

router.post('/createMessage', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // if (req.user.role === 3 || req.user.role === 0) {
    let message = new Message({
        message: req.body.message,
        chat: req.body.chat,
        sender: req.body.sender,
        receiver: req.body.receiver,
        createdAt: moment().format("dd/MM/yyy, hh:mm:ss a"),
    });
    if (validate.validateMessage(message) === true) {
        Message.addNewMessage(message, (err, newMessage) => {
            if (err) {
                utils.errorResponse(res, 500, err);
            } else {
                utils.successResponseWithData(res, newMessage)
            }
        });
    } else {
        utils.errorResponse(res, msg.INVALID_VALIDATE);
    }
    // } else{
    //     utils.errorResponse(res, msg.UNAUTHORIZED);
    // }
});

router.post('/searchMessage', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let searchMessage = req.body.searchMessage;
    Message.findMessage(searchMessage, (err, messages) => {
        console.log(messages);
        if (err) {
            utils.errorResponse(res, 500, err);
        } else {
            utils.successResponseWithData(res, messages)
        }
    });
});

router.post('/getMessagesByChatId', (req, res, next) => {
    const id = req.body.id;
    Message.getMessageByChatId(id, (err, message) => {
        if (err) {
            utils.errorResponse(res, 500, err)
        } else if (!message) {
            utils.errorResponse(res, 404, msg.NOT_FOUND);
        } else {
            utils.successResponseWithData(res, message)
        }
    });
});


module.exports = router;
