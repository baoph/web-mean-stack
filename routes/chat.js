const utils = require("../utils/utils");
const msg = require('../utils/messages');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require("../config/database");
const validate = require('../middleware/validate');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");
const Chat = require('../models/chat');
const fs = require('fs');
const multer = require('multer');


const storage = multer.diskStorage({
    destination: async function (req, file, cb) {
        // closure.getCurrentActive(async (err, closureId) => {
        let uploadTo = './public/uploads/chats';
        if (!fs.existsSync(uploadTo)) {
            await fs.mkdirSync(uploadTo);
            // console.log(uploadTo);
            cb(null, uploadTo);
        } else {
            // console.log(uploadTo);
            cb(null, uploadTo);
        }
        // });
    },
    filename: function (req, file, cb) {
        let arr = file.originalname.split('.');
        let outputLast = arr.pop();
        cb(null, Date.now() + '.' + outputLast);
    }
});
const uploadFolder = multer({storage: storage});

router.post('/createChat', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // if (req.user.role === 3 || req.user.role === 0) {
    let chat = new Chat({
        user: req.body.user,
        restaurant: req.body.restaurant,
    });
    if (validate.validateChat(chat) === true) {
        Chat.addNewChat(chat, (err, newChat) => {
            if (err) {
                utils.errorResponse(res, 500, err);
            } else {
                utils.successResponseWithData(res, newChat)
            }
        });
    } else {
        utils.errorResponse(res, msg.INVALID_VALIDATE);
    }
    // } else{
    //     utils.errorResponse(res, msg.UNAUTHORIZED);
    // }
});

router.get('/getAllChats', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Chat.getAllChat((err, chats) => {
        if (err) {
            utils.errorResponse(res, 500, msg.SERVER_ERROR);
        } else {
            utils.successResponseWithData(res, chats)
        }
    });
});

router.post('/searchChat', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let searchChat = req.body.searchChat;
    Chat.findChat(searchChat, (err, chats) => {
        console.log(chats);
        if (err) {
            utils.errorResponse(res, 500, err);
        } else {
            utils.successResponseWithData(res, chats)
        }
    });
});

router.post('/getChat', (req, res, next) => {
    const id = req.body.id;
    Chat.getChatById(id, (err, chat) => {
        if (err) {
            utils.errorResponse(res, 500, err)
        } else if (!chat) {
            utils.errorResponse(res, 404, msg.NOT_FOUND);
        } else {
            utils.successResponseWithData(res, chat)
        }
    });
});

router.post('/getChatByUserId', (req, res, next) => {
    const id = req.body.id;
    Chat.getChatByUserId(id, (err, chat) => {
        if (err) {
            utils.errorResponse(res, 500, err)
        } else if (!chat) {
            utils.errorResponse(res, 404, msg.NOT_FOUND);
        } else {
            utils.successResponseWithData(res, chat)
        }
    });
});
router.post('/getChatId', (req, res, next) => {
    const params = {
        user: req.body.user,
        restaurant: req.body.restaurant
    }
    Chat.getChatId(params, (err, chat) => {
        if (err) {
            utils.errorResponse(res, 500, err)
        } else if (!chat) {
            utils.errorResponse(res, 404, msg.NOT_FOUND);
        } else {
            utils.successResponseWithData(res, chat)
        }
    });
});

router.post('/updateChat', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // if (req.user.role === 0 || req.user.role === 3){
    let updateChat = {
        chatId: req.body.chatId,
        username: req.body.username,
        tel: req.body.tel,
        email: req.body.email,
        resName: req.body.resName,
        resAddress: req.body.resAddress,
        updatedBy: req.user._id,
        updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a")
    };

    if (updateChat.chatId) {
        if (utils.checkProperties(updateChat)) {
            Chat.updateChat(updateChat, (err, chat) => {
                if (err) {
                    utils.errorResponse(res, 500, msg.SERVER_ERROR);
                } else {
                    utils.successResponseWithData(res, updateChat);
                }
            });
        } else {
            utils.errorResponse(res, 400, msg.INVALID_VALIDATE);
        }
    } else {
        utils.errorResponse(res, -1, msg.NOT_SELECTED);
    }
    // } else{
    //     res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    // }
});

module.exports = router;
