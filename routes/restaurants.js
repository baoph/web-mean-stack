const utils = require("../utils/utils");
const msg = require('../utils/messages');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require("../config/database");
const validate = require('../middleware/validate');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");
const Restaurant = require('../models/restaurant');
const fs = require('fs');
const multer = require('multer');


const storage = multer.diskStorage({
    destination: async function (req, file, cb) {
        // closure.getCurrentActive(async (err, closureId) => {
        let uploadTo = './public/uploads/restaurants';
        if (!fs.existsSync(uploadTo)) {
            await fs.mkdirSync(uploadTo);
            // console.log(uploadTo);
            cb(null, uploadTo);
        } else {
            // console.log(uploadTo);
            cb(null, uploadTo);
        }
        // });
    },
    filename: function (req, file, cb) {
        let arr = file.originalname.split('.');
        let outputLast = arr.pop();
        cb(null, Date.now() + '.' + outputLast);
    }
});
const uploadFolder = multer({storage: storage});

router.post('/createRestaurant', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // if (req.user.role === 3 || req.user.role === 0) {
    let restaurant = new Restaurant({
        username: req.body.username,
        tel: req.body.tel,
        email: req.body.email,
        resName: req.body.resName,
        resAddress: req.body.resAddress,
        created_By: req.user._id,
    });
    if (validate.validateRestaurant(restaurant) === true) {
        Restaurant.addNewRestaurant(restaurant, (err, newRes) => {
            if (err) {
                utils.errorResponse(res, 500, err);
            } else {
                utils.successResponse(res, msg.INSERT_SUCCESSFUL)
            }
        });
    } else {
        utils.errorResponse(res, msg.INVALID_VALIDATE);
    }
    // } else{
    //     utils.errorResponse(res, msg.UNAUTHORIZED);
    // }
});

router.get('/getAllRestaurants', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Restaurant.getAllRestaurant((err, restaurants) => {
        if (err) {
            utils.errorResponse(res, 500, msg.SERVER_ERROR);
        } else {
            utils.successResponseWithData(res, restaurants)
        }
    });
});
// router.post('/getRestaurantByFaculty', (req, res, next) => {
//     let facultyId = req.body.facultyId;
//     Restaurant.getRestaurantByFaculty(facultyId, (err, Faculty) => {
//         if (err) {
//             res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau' });
//         } else {
//             res.json({ code: 300, data: Faculty });
//         }
//     });
// })
router.post('/searchRestaurant', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let searchRestaurant = req.body.searchRestaurant;
    Restaurant.findRestaurant(searchRestaurant, (err, restaurants) => {
        console.log(restaurants);
        if (err) {
            utils.errorResponse(res, 500, err);
        } else {
            utils.successResponseWithData(res, restaurants)
        }
    });
});

router.post('/getRestaurant', (req, res, next) => {
    const id = req.body.id;
    Restaurant.getRestaurantById(id, (err, restaurant) => {
        if (err) {
            utils.errorResponse(res, 500, err)
        } else if (!restaurant) {
            utils.errorResponse(res, 404, msg.NOT_FOUND);
        } else {
            utils.successResponseWithData(res, restaurant)
        }
    });
});

router.post('/updateRestaurant', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // if (req.user.role === 0 || req.user.role === 3){
    let updateRestaurant = {
        restaurantId: req.body.restaurantId,
        username: req.body.username,
        tel: req.body.tel,
        email: req.body.email,
        resName: req.body.resName,
        resAddress: req.body.resAddress,
        updatedBy: req.user._id,
        updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a")
    };

    if (updateRestaurant.restaurantId) {
        if (utils.checkProperties(updateRestaurant)) {
            Restaurant.updateRestaurant(updateRestaurant, (err, restaurant) => {
                if (err) {
                    utils.errorResponse(res, 500, msg.SERVER_ERROR);
                } else {
                    utils.successResponseWithData(res, updateRestaurant);
                }
            });
        } else {
            utils.errorResponse(res, 400, msg.INVALID_VALIDATE);
        }
    } else {
        utils.errorResponse(res, -1, msg.NOT_SELECTED);
    }
    // } else{
    //     res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    // }
});
router.post('/getRestaurantByFaculty', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let facultyId = req.body.facultyId;
    Restaurant.getRestaurantByFaculty(facultyId, (err, restaurant) => {
        if (err) {
            res.json({code: 404, msg: 'Có lỗi trong quá trình thực hiện thao tác. Vui lòng thử lại sau'});
        } else {
            res.json({code: 300, data: restaurant});
        }
    });
});

router.post('/subcribeRestaurant', passport.authenticate('jwt', {session: false}), (req, res, next) => {

});

module.exports = router;
