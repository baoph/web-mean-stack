import {checkProperties} from "../utils/utils";

const utils = require("../utils/utils");

const msg = require('../utils/messages');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const validate = require('../middleware/validate');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");
const Meal = require('../models/meal');
const fs = require('fs');

router.post('/createMeal', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let meal = new Meal({
        mealName: req.body.mealName,
    });
    if (utils.checkProperties(meal) === true) {
        Meal.addNewMeal(meal, (err) => {
            if (err) {
                utils.errorResponse(res, 500, err);
            } else {
                utils.successResponse(res, msg.INSERT_SUCCESSFUL)
            }
        });
    } else {
        utils.errorResponse(res, msg.INVALID_VALIDATE);
    }
});

router.get('/getAllMeals', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Meal.getAllMeal((err, meals) => {
        if (err) {
            utils.errorResponse(res, 500, msg.SERVER_ERROR);
        } else {
            utils.successResponseWithData(res, meals)
        }
    });
});

router.post('/searchMeal', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let mealId = req.body.mealId;
    Meal.getMealById(mealId, (err, meals) => {
        if (err) {
            utils.errorResponse(res, 500, err);
        } else {
            utils.successResponseWithData(res, meals)
        }
    });
});

module.exports = router;
