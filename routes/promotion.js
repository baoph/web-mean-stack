const utils = require("../utils/utils");

const msg = require('../utils/messages');
const express = require('express');
const router = express.Router();
const passport = require('passport');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");
const Promotion = require('../models/promotion');
const fs = require('fs');
const multer = require('multer');

router.post('/createPromotion', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // if (req.user.role === 3 || req.user.role === 0) {
    let promotion = new Promotion({
        promotionName: req.body.promotionName,
        promotionPercent: req.body.promotionPercent,
        promotionFrom: req.body.promotionFrom,
        promotionTo: req.body.promotionTo,
        restaurant: req.body.restaurant,
    });
    console.log(promotion);
    if (utils.checkProperties(promotion) === true) {
        Promotion.addNewPromotion(promotion, (err, newRes) => {
            if (err) {
                utils.errorResponse(res, 500, err);
            } else {
                utils.successResponse(res, msg.INSERT_SUCCESSFUL)
            }
        });
    } else {
        utils.errorResponse(res, msg.INVALID_VALIDATE);
    }
});

router.get('/getAllPromotions', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Promotion.getAllPromotion((err, promotions) => {
        if (err) {
            utils.errorResponse(res, 500, msg.SERVER_ERROR);
        } else {
            utils.successResponseWithData(res, promotions)
        }
    });
});

router.post('/searchPromotion', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let promotionSearch = req.body.promotionSearch;
    Promotion.findPromotionByName(promotionSearch, (err, promotions) => {
        if (err) {
            utils.errorResponse(res, 500, err);
        } else {
            utils.successResponseWithData(res, promotions)
        }
    });
});

router.post('/getPromotionByRestaurant', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let promotionSearch = req.body.restaurantId;
    Promotion.getPromotionByRestaurant(promotionSearch, (err, promotions) => {
        if (err) {
            utils.errorResponse(res, 500, msg.SERVER_ERROR);
        } else {
            utils.successResponseWithData(res, promotions)
        }
    });
});
//getuser promotion by id
router.post('/getPromotionById', (req, res, next) => {
    const id = req.body.id;
    Promotion.getPromotionById(id, (err, promotion) => {
        if (err) throw err;
        if (!promotion) {
            utils.errorResponse(res, 500, msg.NOT_FOUND);
        } else {
            utils.successResponseWithData(res, promotion)
        }

    });
});

module.exports = router;
