﻿const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const Article = require('../models/article');
// const Comment = require('../model/comment');
const validate = require('../middleware/validate');
const moment = require('moment');
const multer = require('multer');
const Users = require('../models/user');
const utils = require("../utils/utils");
const msg = require('../utils/messages');
// const Comments = require('../model/comment');
const mailer = require('../models/mailer');


const fs = require('fs');


const storage = multer.diskStorage({
    destination: async function (req, file, cb) {
        let uploadTo = './public/uploads/restaurants';
        if (!fs.existsSync(uploadTo)) {
            await fs.mkdirSync(uploadTo);
            cb(null, uploadTo);
        } else {
            cb(null, uploadTo);
        }
    },
    filename: function (req, file, cb) {
        let arr = file.originalname.split('.');
        let outputLast = arr.pop();
        cb(null, Date.now() + '.' + outputLast);
    }
});
const uploadFolder = multer({storage: storage});

/**
 * Create new article for restaurant
 */
router.post('/upload/createNewArticle', uploadFolder.array('files'), passport.authenticate('jwt', {session: false}), (req, res, next) => {
    //console.log(uploadTo);
    // if (req.user.role === 1 || req.user.role === 3 || req.user.role === 0 || req.user.role === 2) {
    let newArticle = new Article({
        articleImage: Article.getAllImage(req.files),
        articleContent: req.body.articleContent,
        created_At: moment().format("MMMM Do YYYY, h:mm:ss a"),
        created_By: req.user._id,
        restaurant: req.body.restaurant
    });
    if (utils.validateArticle(newArticle)) {
        Article.insertUploadedFile(newArticle, (err) => {
            if (err) {
                utils.errorResponse(res, 512, msg.SERVER_ERROR);
            } else {
                utils.successResponse(res, msg.INSERT_SUCCESSFUL);
                // Users.getCoordinatorByFaculty(req.body.articleFaculty, (err, coordi) => {
                //     if (err) throw err;
                //     else {
                //         var arti = Object.values(article);
                //         console.log(arti);
                //         let articleid = arti[0]._id;
                //         var result = Object.values(coordi);
                //         let user = result[0].username;
                //         let email = result[0].email;
                //         let postemail = req.body.postemail;
                //         let userpost = req.body.person;
                //         //console.log(result);
                //         const users = [{
                //             name: user,
                //             email: email,
                //             articleid: articleid,
                //             userpost: userpost,
                //             postemail: postemail
                //         }];
                //         console.log(users);
                //
                //         mailer.sendEmailNewArticles(users);
                //     }
                // });
            }
        });
    } else {
        utils.errorResponse(res, 404, msg.INVALID_VALIDATE)
    }
    // } else {
    //     res.status(401).json({code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!'});
    // }
});

router.post('/callSendMailEditArticle', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    const users = [{
        username: req.body.username,
        email: req.body.email,
        articleId: req.body.articleId

    }];
    mailer.sendEmailEditArticles(users);
});
//lấy thông tin tất cả aricle
router.post('/info', (req, res, next) => {
    let facultyId = req.body.facultyId;

    Article.find().populate({
        path: 'articleCategory',
        populate: {path: "belongto_faculty", match: {_id: facultyId}}
    }).populate("created_By").exec(function (err, article) {

        return res.json({article: article});

    });
});


router.post('/searchArticle', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let articleSearch = req.body.articleSearch;
    Article.findArticle(articleSearch, (err, article) => {
        if (err) {
            res.send({code: 404, msg: 'Có lỗi trong quá trình thực hiện thao tác, vui lòng thử lại sau!'});
        } else {
            res.send({code: 300, data: article});
        }
    });
});

router.post('/getUsersArticle', (req, res, next) => {
    let id = req.body.id;

    Article.getUserArticle(id, (err, articles) => {
        if (err) throw err;

        if (!articles) {
            return res.json({success: false, msg: "Khong tim thay"});
        } else {
            return res.json({articles});
        }

    });
});

// coordinator set status cua bai dang
router.post('/editArticleStatus', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    if (req.user.role === 0 || req.user.role === 3) {
        let updateArticleStat = {
            status: req.body.status,
            articleId: req.body.articleId,
            updatedBy: req.body.updatedBy,
            updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a")
        };
        console.log(updateArticleStat);

        if (updateArticleStat.articleId) {
            Article.updateArticleStatus(updateArticleStat, (err, article) => {
                if (err) {
                    res.json({
                        code: 404,
                        msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau!',
                        err: err
                    });
                } else {
                    res.json({code: 300, msg: "Success", data: updateArticleStat});
                }
            });

        } else {
            res.json({code: -1, msg: 'Bạn cần phải chọn 1 bài đăng trước khi thực hiện update!'});
        }
    } else {
        res.status(401).json({code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!'});
    }
});

//student chinh sua bai dang
router.post('/updateArticle', uploadFolder.array('files'), passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // if (req.user.role === 1 || req.user.role === 0) {
    console.log(req.files);
    let imgArr = Article.getAllImage(req.files);
    console.log(req.body.articleImage.split(','));
    req.body.articleImage.split(',').forEach(currentImg => {
        imgArr.push(currentImg);
    });

    let newEditArticle = {
        _id: req.body._id,
        articleImage: imgArr,
        articleContent: req.body.articleContent,
        priceRange: req.body.priceRange,
        category: req.body.category
    };
    if (validate.validateArticle(newEditArticle)) {
        Article.editArticle(newEditArticle, (err, article) => {
            if (err) {
                Article.getAllImage(req.files).forEach(img => {
                    fs.unlinkSync(img);
                })
                utils.errorResponse(res, 500, err);
            } else {
                utils.successResponseWithData(res, article);
            }
        });
    } else {
        utils.errorResponse(res, 400, msg.INVALID_VALIDATE);
    }
    // } else {
    //     res.status(401).json({code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!'});
    // }
});


router.post('/getPostByCategory', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let categoryId = req.body.categoryId;
    let paginationOption = {
        page: parseInt(req.body.page) || 0,
        limit: parseInt(req.body.limit) || 3
    };

    Article.getArticleByCategory(paginationOption, categoryId, (err, article) => {
        if (err) {
            res.send({code: 404, success: false, msg: 'Có vấn đề xảy ra với server. Vui lòng thử lại sau!'});
        } else {
            if (article.length > 0) {
                console.log(article);
                res.json({code: 300, success: true, data: article});
            } else {
                res.json({code: -1, success: false, msg: 'Hết dữ liệu trong hệ thống'});
            }

        }
    });
});

router.post('/getPostByFaculty', (req, res, next) => {
    let facultyId = req.body.facultyId;

    let pageOptions = {
        page: parseInt(req.body.page) || 0,
        limit: parseInt(req.body.limit) || 3
    };

    Article.find({status: "2"}).populate('created_By').populate({
        path: 'articleCategory',
        populate: {
            path: "belongto_faculty",
            match: {_id: facultyId}

        }
    }).sort({_id: -1}).skip(pageOptions.page * pageOptions.limit)
        .limit(pageOptions.limit).exec(function (err, floorplan) {
        if (err) {
            return res.send(err);
        }
        if (!floorplan) {
            return res.status(401).json();
        }
        if (floorplan.length > 0) {
            for (var i = 0; i < floorplan.length; i++) {
                if (floorplan[i].articleCategory.belongto_faculty == null) {
                    delete floorplan[i];
                    console.log(floorplan[i]);

                }
            }
            res.json({code: 300, success: true, data: floorplan});
        } else {
            res.json({code: -1, success: false, msg: 'Hết dữ liệu trong hệ thống'});
        }


    });
});
//get article by nguoi tao
router.post('/getArticle', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let id = req.body.id;
    Article.getArticleById(id, (err, article) => {
        if (err) throw err;
        if (!article) {
            utils.errorResponse(res, 404, msg.NOT_FOUND);
        } else {
            var responseData = {
                _id: article._id,
                articleImage: article.articleImage,
                articleContent: article.articleContent,
                priceRange: article.priceRange,
                category: article.category
            };
            utils.successResponseWithData(res, responseData);
        }
    });
});
//get article by coordinator and student comment
router.post('/getArticleApprovement', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let id = req.body.id;
    Article.getArticleApprovementById(id, (err, article) => {
        if (err) throw err;
        if (!article) {
            return res.json({success: false, msg: "Khong tim thay"});
        } else {

            var responseData = {
                _id: article._id,
                upVote: article.upVote.length,
                downVote: article.downVote.length,
                articleName: article.articleName,
                articleImage: article.articleImage,
                articleContent: article.articleContent,
                filename: article.files,
                upVoteData: article.upVote,
                categoryName: article.categoryName,
                downVoteData: article.downVote,
                created_By: article.created_By,
                articleCategory: article.articleCategory,
                comments: article.comment,
                created_At: article.created_At,
                subTime: moment(article.created_At, "MMMM Do YYYY, h:mm:ss a").fromNow(),
                upDowned: Article.upOrDownVote(article.upVote, article.downVote, req.user._id),
                status: article.status
            };
            //Comment.getCommentById(article.comment._id, (err, cmt) => {
            //    if (err) throw err;

            //    article.comment = cmt;
            //    console.log(cmt);
            //    return 1;
            //}),

            return res.json({article: responseData});
        }

    });
});

router.post('/getIndex', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let paginationOption = {
        page: parseInt(req.body.page) || 0,
        limit: parseInt(req.body.limit) || 3
    };
    Article.getAllArticlesOrderByCreatedDate(paginationOption, (err, articles) => {
        if (err) {
            console.log(err);
            res.send({code: 404, success: false, msg: 'Error with server! Try again Later'});
        } else {
            if (articles.length > 0) {
                console.log(articles);
                res.json({code: 300, success: true, data: articles});
            } else {
                res.json({code: -1, success: false, msg: 'Out of data'});
            }
        }
    });
});


// router.post('/addComment', uploadFolder.array('files'), (req, res, next) => {
//     let commentId = req.body.commentId;
//     if (commentId) { // sub - comment
//         let comment = new Comments({
//             comment: req.body.comment_content,
//             commented_At: moment().format("MMMM Do YYYY, h:mm:ss a"),
//             comment_user: req.body.comment_user,
//             image: Comment.getAllImage(req.files),
//             cmt_status: req.body.status
//         });
//         console.log(comment);
//
//         Comments.addComment(comment, (err, cmt) => {
//             if (err) res.json({ code: 404, msg: 'Có vấn đề xảy ra trong quá trình thực hiện',err:err });
//             else {
//                 if (comment) {
//                     Comment.addSubComment(commentId,cmt._id, (err, article) => {
//                         if (err) res.json({ code: 404, msg: 'Có vấn đề xảy ra', err: err  });
//                         else {
//                             res.json({ code: 300, msg: 'OK' });
//                         }
//                     });
//                 }
//             }
//         });
//     } else { // comment
//         let comment = new Comments({
//             comment: req.body.comment_content,
//             commented_At: moment().format("MMMM Do YYYY, h:mm:ss a"),
//             comment_user: req.body.comment_user,
//             image: Comment.getAllImage(req.files),
//             cmt_status: req.body.status
//
//         });
//         console.log(comment);
//
//         Comments.addComment(comment, (err, cmt) => {
//             if (err) res.json({ code: 404, msg: 'Có vấn đề xảy ra trong quá trình thực hiện', err: err  });
//             else {
//                 if (comment) {
//                     Article.addComment(cmt._id, req.body.articleId, (err, article) => {
//                         if (err) res.json({ code: 404, msg: 'Có vấn đề xảy ra', err: err  });
//                         else {
//                             res.json({ code: 300, msg: 'OK' });
//                         }
//                     });
//                 }
//             }
//         });
//     }
// });
module.exports = router;
