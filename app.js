const express = require('express');
let app = express();
const http = require('http').Server(app);
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const config = require('./config/database');
const passport = require('passport');
const cors = require('cors');
const path = require('path');
const moment_tz = require('moment-timezone');
const users = require('./routes/users');
const restaurants = require('./routes/restaurants');
const articles = require('./routes/articles');
const categories = require('./routes/categories');
const promotions = require('./routes/promotion');
const orders = require('./routes/order');
const chats = require('./routes/chat');
const messages = require('./routes/message');
// Server
// let server = http.createServer(app);
const io = require('socket.io')(http);


moment_tz().tz("Asia/Ho_Chi_Minh");
moment_tz.locale("en");
const port = config.port;

require('dotenv/config');


// Connect to mongoDB

//Connect to mongoDB
mongoose.connect(config.database, {useNewUrlParser: true, useUnifiedTopology: true});

//On Connection
mongoose.connection.on('connected', () => {
    console.log('Connected to database', config.database);
});
//On error
mongoose.connection.on('error', (err) => {
    console.log('Database Error: ' + err);
});

// bodyParser
app.use(bodyParser.json({limit: '500mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));
//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);
app.use("/public", express.static(path.join(__dirname, 'public')));
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

// Cors
app.use(
    cors({
        origin: 'http://localhost:4200',
        credentials: true,
        optionsSuccessStatus: 204,
        exposedHeaders: ['File-Name'],
    }),
);

app.use('/testTimezone', (req, res, next) => {
    let a = moment_tz().tz("Asia/Ho_Chi_Minh").format('MMMM Do YYYY, h:mm:ss a');
    console.log(a);
    res.json({time: a, zone: moment_tz().tz("Asia/Ho_Chi_Minh").zoneName()})
});

app.use('/users', users);
app.use('/restaurant', restaurants);
app.use('/article', articles);
app.use('/category', categories);
app.use('/promotion', promotions);
app.use('/order', orders);
app.use('/chat', chats);
app.use('/message', messages);

let messageArr = [];

io.on('connection', socket => {
    let previousId;
    const safeJoin = currentId => {
        socket.leave(previousId);
        socket.join(currentId, () => console.log(`Socket ${socket.id} joined room ${currentId}`));
        previousId = currentId;
    }

    socket.on('getDoc', message => {
        safeJoin(message._id);
        socket.emit('document', message.message);
    });

    socket.on('addDoc', doc => {
        console.log(doc);
        // documents[doc.id] = doc;
        chatIdArr.push(doc._id);
        safeJoin(doc._id);
        io.emit('documents', chatIdArr);
        socket.emit('document', doc);
    });

    socket.on('addMessage', doc => {
        console.log(doc);
        messageArr.push(doc);
        safeJoin(doc._id);
        io.emit('messages', messageArr);
        socket.emit('messages', messageArr);
    });

    socket.on('getMessageByChatId', docs => {
        messageArr = [...docs];
        io.emit('messages', messageArr);
    })

    socket.on('getDocsByRestaurantId', docs => {
        messageArr = [...docs];
        io.emit('documents', chatIdArr);
    })


    console.log(`Socket ${socket.id} has connected`);
});

http.listen(port, () => {
    console.log(`the application is running on port ${port}`);
});


